import api.dto.response.books.BookResponseDTO;
import api.exceptions.BookNotFoundException;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import api.service.BookService;
import api.steps.BookSteps;
import api.steps.UserSteps;
import io.qameta.allure.*;
import static java.lang.String.format;
import static org.assertj.core.api.Assertions.assertThat;

@Epic("This is general epic")
public class BasicTest {

    @BeforeMethod(groups = {"Regression", "Smoke", "Api"})
    void clearAllBooks() {
        new BookSteps().deleteBook();
    }

    @Test(description = "Get user with book or not", groups = {"Regression", "Api"})
    @Story("[1232-xml] As a user I want to get my own book user")// for PR comment
    @Link(name = "[1234-xml]", url = "https://google.com")
    @Issue("AUTH-123")

    void shouldBeUserReturned() {
        var user = new UserSteps().getUser();
        assertThat(user.getUsername()).as("Юзернейм не коректний").isEqualTo(System.getenv("TEST_USER_NAME"));
//        assertThat(user.getUsername()).as("Юзернейм не коректний").isEqualTo("user15");
    }

    @Test(dataProvider = "ChangeBooks", groups = {"Smoke", "Api"})
    @Story("As a user I want to change one book to another")
    @Flaky
    @Step
    void shouldBeAddedOneBookIntoUser(String booksTitle1, String booksTitle2) {
        var isbn1 = new BookService().getIsbnOfBook(booksTitle1);
        var isbn2 = new BookService().getIsbnOfBook(booksTitle2);

        new BookSteps().addBookIntoUser(isbn1);

        var user = new UserSteps().getUser();

        var bookTitle = user.getBooks().stream().map(BookResponseDTO::getTitle).filter(title -> title.equals(booksTitle1))
                .findFirst().orElseThrow(() -> new BookNotFoundException(format("Книгу - %s не знайдено", booksTitle1)));

        assertThat(bookTitle).as("Назва не коректна").isEqualTo(booksTitle1);

        new BookSteps().deleteBook();

        new BookSteps().addBookIntoUser(isbn2);

        var updUser = new UserSteps().getUser();

        var updBookTitle = updUser.getBooks().stream().map(BookResponseDTO::getTitle)
                .filter(title -> title.equals(booksTitle2))
                .findFirst()
                .orElseThrow(() -> new BookNotFoundException(String.format("Книгу - %s не знайдено", booksTitle2)));
        assertThat(updBookTitle).as("Назва не коректна").isEqualTo(booksTitle2);

    }

//    @Test(dataProvider = "WrongBooks", groups = {"Smoke", "Api"})
//    void shouldBeValidatedBookTitle(String s1, Integer s2) {
//        var empty = new BookService().checkIsbnBook(s1);
//        System.out.println(s2);
//        assertThat(empty).isEqualTo(true);
//    }
//
//    @DataProvider(name = "WrongBooks")
//    public Object[][] getWrongBooks() {
//        return new Object[][] {
//                {"", 343},
//                {"---", 111},
//                {"123", 145}
//        };
//    }

    @DataProvider(name = "Books")
    public Object[][] getBooks() {
        return new Object[][] {
                {"You Don't Know JS"},
                {"Learning JavaScript Design Patterns"},
                {"Git Pocket Guide"}
        };
    }
    @DataProvider(name = "ChangeBooks")
    public Object[][] changeBooks() {
        return new Object[][] {
                {"You Don't Know JS", "Designing Evolvable Web APIs with ASP.NET"},
                {"Learning JavaScript Design Patterns", "Speaking JavaScript"},
                {"Git Pocket Guide", "Programming JavaScript Applications"}
        };
    }
}
///