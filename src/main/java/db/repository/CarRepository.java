package db.repository;

import db.dao.Car;

import java.util.List;

public interface CarRepository {

    Car getCarByBrand(String username);
    Car getCarById(Long id);
    Car getCarByBrandAndYear(String username, Integer age);
    List<Car> getAllCars();
    void saveCar(Car car);
    void deleteCar(String carbrand);
    void deleteCarById(Long id);
    void updateCar(Car car, String findCar);
    void updateCarBrand(String newCarBrand, String oldCarBrand);
}
