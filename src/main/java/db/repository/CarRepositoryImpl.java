package db.repository;

import db.config.StatementInit;
import db.dao.Car;
import db.exceptions.CarNotFoundEx;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CarRepositoryImpl implements CarRepository {
    @Override
    public Car getCarByBrand(String carbrand) {
        try {
            String sql = "SELECT * FROM car WHERE brand=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, carbrand);

            ResultSet rs = preparedStatement.executeQuery();

            if (rs.next()) {
                return new Car(rs.getString("brand"), rs.getInt("year"), rs.getString("model"));
            } else {
                throw new CarNotFoundEx("Car from data base is not found");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Car getCarById(Long id) {
        String sql = "SELECT * FROM car WHERE id=?";
        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, id);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new Car(rs.getString("brand"), rs.getInt("year"), rs.getString("model"));
            } else {
                throw new CarNotFoundEx("Car from data base is not found");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Car getCarByBrandAndYear(String carbrand, Integer year) {
        try {
            String sql = "SELECT * FROM car WHERE brand=? and year=?";
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, carbrand);
            preparedStatement.setInt(2, year);

            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                return new Car(rs.getString("brand"), rs.getInt("year"), rs.getString("model"));
            } else {
                throw new CarNotFoundEx("Car from data base is not found");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Car> getAllCars() {
        List<Car> cars = new ArrayList<>();

        try {
            ResultSet rs = StatementInit.getConnection().createStatement().executeQuery("SELECT * FROM car");
            while (rs.next()) {
                cars.add(new Car(rs.getString("brand"), rs.getInt("year"), rs.getString("model")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return cars;
    }

    @Override
    public void saveCar(Car car) {
        String sql = "INSERT INTO car (brand, year, model) VALUES (?,?,?)";
        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setInt(2, car.getYear());
            preparedStatement.setString(3, car.getModel());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void deleteCar(String carbrand) {
        String sql = "DELETE FROM car WHERE brand=?";
        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, carbrand);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void deleteCarById(Long id) {
        String sql = "DELETE FROM car WHERE id=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setLong(1, id);

            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void updateCar(Car car, String findCar) {
        String sql = "UPDATE car SET brand=?, year=?, model=? WHERE brand=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, car.getBrand());
            preparedStatement.setInt(2, car.getYear());
            preparedStatement.setString(3, car.getModel());
            preparedStatement.setString(4, findCar);
            preparedStatement.executeUpdate();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void updateCarBrand(String newCarBrand, String oldCarBrand) {
        String sql = "UPDATE car SET brand=? WHERE brand=?";

        try {
            PreparedStatement preparedStatement = StatementInit.getConnection().prepareStatement(sql);
            preparedStatement.setString(1, newCarBrand);
            preparedStatement.setString(2, oldCarBrand);
            preparedStatement.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }
}
