package db.exceptions;

public class CarNotFoundEx extends RuntimeException {
    public CarNotFoundEx(String message) {
        super(message);
    }
}
